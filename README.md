# vim-ale-example-react

A tiny example of a [React][react] app with the formatter
[Prettier][prettier] and linter [ESLint][eslint] in Vim using the Vim
plugin [ALE][ale].

### Screencast

[![Screencast of example](https://img.youtube.com/vi/WctpnNjCnEI/0.jpg)](https://www.youtube.com/watch?v=WctpnNjCnEI)

### Steps

Tested on
* Prettier v.2.7.1
* ESLint v.8.28.0
* Vim v.9.0
* ALE v.3.20 
* React v.18.2.0

```bash
npx create-react-app vim-ale-example-react
cd vim-ale-example-react
npm i -D eslint eslint-plugin-prettier eslint-plugin-react prettier
echo -e "build\ncoverage" > .prettierignore
echo '{
  "semi": false,
  "singleQuote": true
}' > .prettierrc.json
echo '{
  "env": {
    "browser": true,
    "es2021": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  "overrides": [
  ],
  "parserOptions": {
    "ecmaVersion": "latest",
    "sourceType": "module"
  },
  "plugins": [
    "react",
    "prettier"
  ],
  "rules": {
    "prettier/prettier": "error",
    "react/prop-types": "off",
    "react/react-in-jsx-scope": "off",
    "no-debugger": "off"
  }
}' > .eslintrc.json
npx prettier -wc src
```

### Vim config

Make sure your .vimrc contains:

```vimscript
let g:ale_linters = {
\   'javascript': ['eslint'],
\}
let g:ale_fixers = {
\   'javascript': ['prettier'],
\   'css': ['prettier'],
\}
nmap <silent> <leader>f <Plug>(ale_fix)
```

Remember to have your Node version available in the shell that you use
for Vim.

[ale]: https://github.com/dense-analysis/ale
[eslint]: https://eslint.org/
[prettier]: https://github.com/sheerun/prettier-standard
[react]: https://github.com/facebook/react/
