const helper = (...args) => {
  return args.join(' ');
}

function Example () {
  return (
    <p>
      {helper('reallyLongArg', 'omgSoManyParameters', 'IShouldRefactorThis', 'isThereSeriouslyAnotherOn')}
    </p>
  );
}

export default Example;
